# SIMPLE JSON REST Server

## Setup
```
git clone https://bitbucket.org/lzaugg/simple-json-rest-server.git
cd simple-json-rest-server
npm install
```

## Running

Just start with argument data-dir where data-dir is a directory with json resources in it.

### Usage
```
> node ./app/server.js

Respond RESTful with JSON data from a data directory.
Usage: node ./app/server.js <datadir>

Options:
  -p, --port   listening port      [default: 8080]
  -s, --psize  paging size to use  [default: 50]

<datadir>: data directory to use as source for JSON files

```

### Data Dir
e.g.
```
data/resource1.json
data/resource1/
data/resource1/123.json
data/resource2.json
```

```
GET /resource1
-> resource1.json
GET /resource1/938
-> ERROR (not found)
GET /resource1/123
-> resource1/123.json
GET /resource2
-> resource2.json
GET /resource3
-> []
```

### Start
```
npm start <data-dir>
```