'use strict'

var restify = require('restify');
var jf = require('jsonfile');
var dir = require('node-dir');
var argv = require('yargs')
    .usage('Respond RESTful with JSON data from a data directory.\nUsage: $0 <datadir>')
    .default('p', 8080)
    .alias('p', 'port')
    .describe('p', 'listening port')
    .default('s', 50)
    .alias('s', 'psize')
    .describe('s', 'paging size to use')
    .demand(1,'<datadir>: data directory to use as source for JSON files')
    .argv;


var pagingSize = argv.s;
var port = argv.p;
var dataDir = argv._[0];


console.log('using data directory: ' + dataDir);
console.log('start server on port: ' + port);
console.log('use paging size: ' + pagingSize);

function respond(req, res, next) {
  var log = req.log;
  log.debug({params: req.params}, 'Hello there %s', 'foo');
  var rName = req.params.resource;
  var rId = req.params.id;
  var rPath = dataDir + '/' + rName;
  var file;
  var page = req.params.page;
  
  console.log('got req: ' + JSON.stringify(req.params));
  if (rName) {
    if (rId) {
      // entity
      rId = req.params.id;
      file = rPath + '/' + rId + '.json';
      jf.readFile(file, function(err, obj) {
        if (err) {
          next(new Error('no such resource with id "' +rId+'"" found: ' + file));
        } else {
          res.send(obj);
          next();
        }
      });
    } else {
      // entities
      file = rPath + '.json';
      jf.readFile(file, function(err, obj) {
        var fromIndex;
        var toIndex;
        var entities = [];
        if (err) {
          dir.readFiles(rPath, function(err, content, nextF) {
            if (!err) {
              entities.push(JSON.parse(content));
            }
            nextF();
          }, function(err) {
            if (err) {
              next(err);
            }
            res.send(entities);
          });
          
        } else {
          if (!page) {
            page = 1;
          }
          fromIndex = (page - 1) * pagingSize;
          toIndex = fromIndex + pagingSize;
          res.send(obj.slice(fromIndex,toIndex));
        }
        next();
      });
    }
  } else {
    next(new Error('no resource given in request'));
  }
}

var server = restify.createServer();
server.use(restify.queryParser());
server.use(restify.CORS());
server.use(restify.fullResponse());
server.get('/:resource', respond);
server.get('/:resource/:id', respond);

server.listen(port, function() {
  console.log('%s listening at %s', server.name, server.url);
});